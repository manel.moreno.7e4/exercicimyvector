﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExInicialCSharp
{
    public static class VectorGame
    {
        public static void VectorGameProcess()
        {
            Console.WriteLine("How many vectors do you want to generate bro?");
            var vectorsCount = MenuUtils.ProcessUserInput(false);

            Console.WriteLine("How do you want to sort them?\n" +
                "\t 1. Distance" +
                "\t 2. Origin proximity");
            var count = 1;
            switch(MenuUtils.ProcessUserInput(true))
            {
                case 1:
                    foreach (var array in SortByDistance(RandomVectors((int)vectorsCount)))
                    {
                        Console.WriteLine($"\nVector {count}:\n" +
                            $"{MyVector.ToString(array)}\n" +
                            $"Vector distance: {MyVector.GetDistance(array)}");
                        count++;
                    }
                    break;
                case 2:
                    foreach (var array in SortByOriginProximity(RandomVectors((int)vectorsCount)))
                    {
                        Console.WriteLine($"\nVector {count}:\n" +
                            $"{MyVector.ToString(array)}\n" +
                            $"Origin Proximity: {MyVector.GetOriginProximity(array)}");
                        count++;
                    }
                    break;
                default://If user presses 3,4 or 5
                    Console.WriteLine("Wrong input, limit yourself to press 1 or 2 please");
                    break;
            }
        }

        private static MyVector[] RandomVectors(int length)
        {
            Random rand = new Random();
            List<MyVector> vectorsList = new List<MyVector>();
            for (int i = 0; i < length; i++)
            {
                vectorsList.Add(new MyVector(new float[2] { rand.Next(), rand.Next() }, new float[2] { rand.Next(), rand.Next() }));
            }
            return vectorsList.ToArray();
        }

        private static MyVector[] SortByDistance(MyVector[] arrayVectors)
        {
            Console.WriteLine("Sorting vectors by distance");
            return arrayVectors.OrderBy(x => MyVector.GetDistance(x)).ToArray();
        }

        private static MyVector[] SortByOriginProximity(MyVector[] arrayVectors)
        {
            Console.WriteLine("Sorting vectors by origin proximity");
            return arrayVectors.OrderBy(x => MyVector.GetOriginProximity(x)).ToArray();
        }
    }
}
