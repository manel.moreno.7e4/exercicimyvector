﻿using System;

namespace ExInicialCSharp
{
    public class MyVector
    {
        private static MyVector _currentVector = null;
        private float[] _origin = null;
        private float[] _final = null;

        public float[] Origin { get => _origin; set => _origin = value; }
        public float[] Final { get => _final; set => _final = value; }

        public MyVector(float[] origin, float[] final)
        {
            Origin = origin;
            Final = final;
        }

        public static void ShowVectorInfo()
        {
            if (_currentVector != null)
            {
                Console.WriteLine(ToString(_currentVector));
                Console.WriteLine($"Current vector distance: {_currentVector.GetDistance()}");
            }
            else
            {
                Console.WriteLine("You haven't defined a Vector yet!");
            }

        }

        public static void UpdateCurrentVector()
        {
            _currentVector = new MyVector(AskForVectorInfo("origin"), AskForVectorInfo("final"));
        }

        private static float[] AskForVectorInfo(string pointRef)
        {
            Console.WriteLine($"Define the {pointRef} point: ");
            Console.WriteLine("X value: ");
            var xValue = MenuUtils.ProcessUserInput(false);
            Console.WriteLine("Y value: ");
            var yValue = MenuUtils.ProcessUserInput(false);
            return new float[2] { xValue, yValue };
        }

        public float GetDistance()
        {
            return (float)Math.Sqrt(Math.Pow(Origin[1] - Origin[0], 2) + Math.Pow(Final[1] - Final[0], 2));
        }

        public static float GetDistance(MyVector vector)
        {
            return (float)Math.Sqrt(Math.Pow(vector.Origin[1] - vector.Origin[0], 2) + Math.Pow(vector.Final[1] - vector.Final[0], 2));
        }

        public static float GetOriginProximity(MyVector vector)
        {
            return (float)Math.Sqrt(Math.Pow(vector.Origin[0], 2) + Math.Pow(vector.Origin[1], 2));
        }

        public void ChangeDirection()
        {
            float[] bubble = Final;
            Final = Origin;
            Origin = bubble;
        }

        public static string ToString(MyVector vector)
        {
            return $"Origin point:({vector.Origin[0]}, {vector.Origin[1]})\n" +
                $"Final point: ({vector.Final[0]}, {vector.Final[1]})";
        }
    }
}
