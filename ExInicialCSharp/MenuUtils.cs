﻿using System;

namespace ExInicialCSharp
{
    public class MenuUtils
    {
        public const string WrongInputMessage = "Wrong input!!!!!!!!!!!!!!";
        public static void HelloName()
        {
            Console.WriteLine("Input your name: ");
            var input = Console.ReadLine();
            Console.WriteLine("Hello " + input);
        }

        public static float ShowMenu()
        {
            Console.WriteLine("Input a number to choose an option:\n" +
                "\t1. Hello (name)\n" +
                "\t2. Define a Vector\n" +
                "\t3. Show Vector info\n" +
                "\t4. Generate and sort random vectors" +
                "");
            var userOption = ProcessUserInput(true);
            switch (userOption)
            {
                case 1:
                    HelloName();
                    break;
                case 2:
                    MyVector.UpdateCurrentVector();
                    break;
                case 3:
                    MyVector.ShowVectorInfo();
                    break;
                case 4:
                    VectorGame.VectorGameProcess();
                    break;
                case 5:
                    break;
            }
            return userOption;
        }

        public static string Pause()
        {
            Console.WriteLine("Press enter to return to menu, or intro \"5\" to exit the program");
            return Console.ReadLine();
        }

        public static float ProcessUserInput(bool applyRange)
        {
            float userFloat;
            var validInput = true;

            if (!float.TryParse(Console.ReadLine(), out userFloat))
            {
                validInput = WrongInputSequence();
            }

            if (applyRange)
            {
                if (userFloat < 0 || userFloat > 5)
                {
                    validInput = WrongInputSequence();
                }
            }

            if (!validInput)
            {
                userFloat = AskInputAgain(applyRange);
            }
            return userFloat;
        }

        private static float AskInputAgain(bool applyRange)
        {
            Console.WriteLine("Try again: ");
            return ProcessUserInput(applyRange);
        }

        private static bool WrongInputSequence()
        {
            Console.WriteLine(WrongInputMessage);
            return false;
        }
    }
}
